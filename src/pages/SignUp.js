import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity,

} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
export default class signUp extends React.Component {

    render() {
        return (
<ScrollView>
            <View style={styles.container}>

                <Image source={require('../images/Login.png')} resizeMode = 'stretch' style = {styles.backdrop} />

                <View style={styles.textView}>
                    <Text style={{color:'#ffffff',fontSize:20}}>Sign Up</Text>
                    <Text style={styles.textStyle}>Please signup to listen unforgettable stories</Text>
                    <TextInput style={styles.inputtext}    placeholder = "First Name * " underlineColorAndroid='transparent'/>
                    <TextInput style={styles.inputtext}    placeholder = "Last Name" underlineColorAndroid='transparent'/>
                    <TextInput style={styles.inputtext}    placeholder = "Age " underlineColorAndroid='transparent'/>
                    <TextInput style={styles.inputtext}    placeholder = "E-mail * " underlineColorAndroid='transparent'/>
                    <TextInput style={styles.inputtext} secureTextEntry={true}
                               placeholder="Password"
                               underlineColorAndroid='transparent'/>
                    <TextInput style={styles.inputtext} secureTextEntry={true}
                               placeholder="Confirm Password *"
                               underlineColorAndroid='transparent'/>
                    <View style={{ flexDirection: 'row',marginTop:20}}>
                        <CheckBox/>
                        <Text style={{color:'#ffffff',fontSize:13,paddingHorizontal:10,marginTop:5}}>I agree to Term & Conditions.</Text>
                    </View>

                    <TouchableOpacity onPress={this._onPressButton} style={{
                        marginTop: 25,
                        flexDirection: 'row',
                        height: 50,
                        justifyContent: 'center',
                        backgroundColor: '#60BA45',
                        paddingTop: 15,
                        borderRadius: 10,
                    }}>
                        <Text style={{color:'#ffffff'}}>Sign Up</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row',marginTop:12,paddingHorizontal:100}}>
                        <Text style={styles.account}>Already an User? </Text>

                        <TouchableOpacity style={{height: 50,}}><Text style={{color:'#F07E7C'}}>Sign In</Text></TouchableOpacity>

                    </View>



                </View>{/*</View>*/}

                {/*</View>*/}
            </View>
</ScrollView>
        );
    }
}


var styles = StyleSheet.create({
    backgroundContainer: {
        position: 'relative',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

    },
    container: {
        flex: 1,
        alignItems: 'stretch',
        //   justifyContent:'center',
        //position: 'relative'
    },

    backdrop: {
        flex:1,
        //flexDirection: 'row',
        //height:'100%',
        width: '100%',
        //position:'absolute',
        alignItems: 'center',
        justifyContent:'center'
    },
    textView:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        //justifyContent: 'center',
        //alignItems: 'center'
        marginTop:130,
        paddingHorizontal:30,
    },
    textStyle:{
        fontSize: 16,
        color: '#ffffff'
    },
    inputtext:{
        marginTop:20,
        height: 50,
        borderColor: '#ffffff',
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderRadius:10,
        fontSize: 18,
        paddingHorizontal : 16,

    },
    account: {
        color: '#ffffff'



    },
});
