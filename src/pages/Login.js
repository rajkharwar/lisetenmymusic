import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity

} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';

export default class Login extends React.Component {

    render() {
        return (

             <View style={styles.container}>

                    <Image source={require('../images/Login.png')} resizeMode = 'stretch' style = {styles.backdrop} />

                 <View style={styles.textView}>
                     <Text style={styles.textStyle}>Listen stories by ear, cherish by Heart</Text>
                     <TextInput style={styles.inputtext}    placeholder = "E-mail address " underlineColorAndroid='transparent'/>
                     <TextInput style={styles.inputtext} secureTextEntry={true}
                                 placeholder="Password"
                                underlineColorAndroid='transparent'/>

                     <TouchableOpacity onPress={this._onPressButton} style={{
                         marginTop: 25,
                         flexDirection: 'row',
                         height: 50,
                         justifyContent: 'center',
                         backgroundColor: '#EC4187',
                         paddingTop: 20,
                         borderRadius: 10,
                     }}>
                         <Text style={{color:'#ffffff'}}>LOG IN</Text>
                     </TouchableOpacity>
                     <View style={{flexDirection:'row'}}>
                         <TouchableOpacity style={{height: 50,marginTop:8}}><Text style={styles.Forgot}>Forgot Password?</Text></TouchableOpacity>
                     </View>
                     <TouchableOpacity onPress={this._onPressButton} style={{
                             marginTop: 150,
                             flexDirection: 'row',
                             height: 50,
                             justifyContent: 'center',
                             backgroundColor: '#B9B9B9',
                             paddingTop: 15,
                             borderRadius: 10,
                         }}>
                             <Text style={{color:'#ffffff'}}>REGISTER NEW ACCOUNT</Text>
                         </TouchableOpacity>


                 </View>{/*</View>*/}

                  {/*</View>*/}
              </View>
        );
    }
}


var styles = StyleSheet.create({
    backgroundContainer: {
        position: 'relative',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

    },
    container: {
        flex: 1,
        alignItems: 'stretch',
     //   justifyContent:'center',
        //position: 'relative'
    },

    backdrop: {
        flex:1,
        //flexDirection: 'row',
        //height:'100%',
        width: '100%',
        //position:'absolute',
        alignItems: 'center',
        justifyContent:'center'
   },
    headline: {
        fontSize: 18,
        textAlign: 'center',
        backgroundColor: 'black',
        color: 'white'
    },
    textView:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        //justifyContent: 'center',
        //alignItems: 'center'
        marginTop:130,
        paddingHorizontal:30,
    },
    textStyle:{
        fontSize: 20,
        color: '#ffffff'
    },
    inputtext:{
        marginTop:20,
        height: 50,
        borderColor: '#ffffff',
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderRadius:10,
        fontSize: 18,
        paddingHorizontal : 16,

    },
    Forgot :{
        color: '#ffffff',
        fontSize: 15,
        paddingLeft:100,

    },
});
