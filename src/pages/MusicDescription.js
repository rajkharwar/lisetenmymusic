import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity, FlatList,TouchableWithoutFeedback,

} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';

export default class MusicDescription extends React.Component {

    render() {
        return (

            <View style={styles.container}>

                <Image source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />

                <View style={styles.textView}>
                    <View style={styles.RecoverPass}>

                        <Text style={{color:'#ffffff',fontSize:25}}>Description</Text>
                    </View>
                    <Text style={{color:'#ffffff',marginTop:10,fontSize: 15,letterSpacing:.5,fontFamily:'Nunito Sans',lineHeight:20}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>

                    <View style={styles.RecoverPass}>

                        <Text style={{color:'#ffffff',fontSize:20,paddingTop:15,paddingBottom:10}}>101 - The Random Thoughts</Text>
                    </View>
                    <View>
                    <Image source={require('../images/rating.png')} style={{height:25,width:250}}/>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        height: 110,
                        paddingHorizontal:20,
                        backgroundColor: '#ffffff',
                        paddingTop: 10,
                        borderRadius: 10,

                    }}>
                        <View title="DEFAULT">
                            <Text style={{fontSize:18,color:'#5E6A6E',paddingBottom:10}}>Rate This Product</Text>
                            <AirbnbRating showRating={false} />
                        </View>
                    </View>
                    <View style={styles.RecoverPass}>

                        <Text style={{color:'#ffffff',fontSize:20,paddingTop:15,paddingBottom:10}}>Review this Podcast</Text>
                    </View>
                    <View style={{borderWidth:2, borderRadius: 8, borderColor: '#D9D9D9',}}>
                        <View style={{borderBottomWidth:1, borderColor: '#D9D9D9'}}>
                        <TextInput style={styles.inputSearch}  placeholder = "Title..." placeholderTextColor='#ffffff' underlineColorAndroid='transparent'/>
                        </View>
                        <View>
                            <TextInput style={styles.descSearch} placeholder = "Description..." placeholderTextColor='#ffffff' underlineColorAndroid='transparent'/>
                        </View>
                    </View>
                    <View  style={{
                        flex:1,
                        marginTop: 20,
                        flexDirection: 'row',
                        height: 50,
                        paddingHorizontal:20,
                        backgroundColor: '#ffffff',
                        paddingTop: 10,
                        //borderRadius: 10,
                    }}>
                        <Text style={{color:'#4A4A4A',fontSize:20}}>Ratings & Reviews</Text>
                    </View>




                </View>
                {/*<View style={{flex:1}}>*/}
                {/*    <FlatList*/}
                {/*        data={[*/}
                {/*            {key: '101 - The Random Thoughts'},{key: '102 - The Random Thoughts'}, {key: '103 - The Random Thoughts'},{key: 'Swift'},*/}
                {/*            {key: 'Php'},{key: 'Hadoop'},{key: 'Sap'},*/}

                {/*        ]}*/}
                {/*        renderItem={({item}) =>*/}

                {/*            // <View style={{justifyContent:'space-between',marginTop:1}}>*/}

                {/*            <View style={{flexDirection:'row',height: 70,borderBottomWidth:2,flex:1}}>*/}

                {/*                <Text style={styles.item}>{item.key}*/}
                {/*                </Text>*/}
                {/*            </View>*/}

                {/*            // </View>*/}

                {/*        }*/}
                {/*        keyExtractor={item => item.key}*/}
                {/*        ItemSeparatorComponent={this.renderSeparator}*/}
                {/*        contentContainerStyle={{flexGrow:1,marginTop:1}}*/}
                {/*    />*/}
                {/*</View>*/}
            </View>

        );
    }
}


var styles = StyleSheet.create({
    backgroundContainer: {
        position: 'relative',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

    },
    container: {
        flex: 1,
        alignItems: 'center',

    },

    backdrop: {
        flex:1,
        width: '100%',

    },

    textView:{
        position: 'absolute',
        marginTop:20,
        paddingHorizontal:20,

    },
    textStyle:{
        fontSize: 20,
        color: '#ffffff',
        marginTop:30
    },
    RecoverPass:{
        flexDirection: 'row',
        justifyContent:'space-between',

    },
    inputSearch: {
        height: 46,
        padding: 10,
       // margin: 14,
        fontSize: 15,
       // borderWidth: 2,
        borderRadius: 8,
        borderColor: '#D9D9D9',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    descSearch:{
        height: 100,
        padding: 10,
        //margin: 14,
        fontSize: 15,
       // borderWidth: 1,
        borderRadius: 8,
        borderColor: '#D9D9D9',
        backgroundColor: 'rgba(0,0,0,0)',
    }



});
