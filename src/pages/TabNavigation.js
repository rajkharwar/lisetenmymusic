import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image} from 'react-native';



export default class TabNavigation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <View style={{flexDirection:'column',backgroundColor: '#1F253C'}}>
                <View style={styles.container}>

                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <View style={styles.viewStyleOne}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Text style={styles.textStyle}> Personal Detail</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.viewStyleTwo}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Text style={styles.textStyle}> Work Schedule </Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.viewStyleThree}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Text style={styles.textStyle}>Bank Detail</Text>
                            </TouchableOpacity>


                        </View>
                    </View>


                    <View style={{flexDirection:'column',justifyContent:'space-around'}}>
                        <View style={styles.viewStyleOne}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Text style={styles.textStyle}>Expriences / DL </Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.viewStyleTwo}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Text style={styles.textStyle}>Pay Scale </Text>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.viewStyleThree}>
                            <TouchableOpacity style={styles.iconStyle} activeOpacity={0.5}>
                                <Text style={styles.textStyle}>Vechile Prefrence </Text>
                            </TouchableOpacity>


                        </View>
                    </View>


                </View>
                <View style={{justifyContent:'space-around',paddingHorizontal: 20,paddingTop:10}}>
                    <TouchableOpacity onPress={this._onPressButton} style={{marginBottom:23,flexDirection:'row',height: 50,justifyContent: 'center',backgroundColor:'#00CB99',paddingTop:20,borderRadius:20}}>
                        <Text style={{color:'#ffffff'}}>Okay, got it</Text>
                        <Image
                            source={require('../images/arrow.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>





        );

    }
}
//Styles
const styles = StyleSheet.create({
    container: {
        paddingTop:10,
        paddingHorizontal:16,
        // flex:1,
        height:550,
        justifyContent:'space-around',
        flexDirection: 'row',

    },
    textPro : {
        color: '#ffffff',
        fontWeight:'bold' ,
        fontSize: 20,
        paddingTop: 10,
        paddingHorizontal:29,
    },

    viewStyleOne: {
        width:160,
        height:170,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:13,


    },
    viewStyleTwo: {
        width:160,
        height:170,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:13,

    },
    viewStyleThree: {
        width:160,
        height:170,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:13,
    },
    textStyle:{
        textAlign:'center'
    },

    iconStyle: {
        alignItems: 'center',
        // height: 15,
        //width: 10,
    },
    instructions: {
        fontSize: 22,
        fontWeight: '600',
        color: '#FFFFFF',
        marginBottom: 20,
    },
})




