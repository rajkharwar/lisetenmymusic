import { createAppContainer } from "react-navigation";
import {createBottomTabNavigator} from 'react-navigation-tabs';
import MusicList from './MusicList';
import Login from './Login';
import SignUp from './SignUp';

const Tab = createBottomTabNavigator({
    Login: {
        screen: Login
    },
    SignUp: {
        screen: SignUp
    },

}, {
    initialRouteName:'Login',
    tabBarPosition: 'bottom', // As it says, you will configure it to appear on bottom.
    swipeEnabled: true, // This one make you through the screens
    tabBarOptions: { // Here we add some options to customize our tab as default.
        activeTintColor: '#f2f2f2', // As the name says: The color that the active tab will have.
        activeBackgroundColor: '#4348AB', // The background color of the active tab
        inactiveTintColor: '#666', // The inactive tab color. It will be the color of the text
        labelStyle: {// The label style. Here i just made a change on size and padding
            fontSize: 20,
            padding: 5,

        },

    }
});

export default createAppContainer(Tab);
