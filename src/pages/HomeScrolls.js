import React ,{Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    FlatList, Image, Alert
} from 'react-native';

export default class HomeScrolls extends React.Component{
   constructor(props) {
       super(props);
   }
data1="Love";
data2="Life";
data3="Education";
data4="Relation";
data5="Drama";
    render()
    {
        return(
            <View style={{height:110,marginTop:5}}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}

                >
                    {
                            this.props.temp.map((item,i) => (
                                <View key = {i} style={styles.items}>
                                    <Image source={require('../images/health.png')} style={{width:50,height:50,marginTop:10}}/>
                                    <Text>{item.Name}</Text>
                                </View>
                            ))
                        }

                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
    },
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    items:{

        minWidth: 90,
        maxWidth: 90,
        height: 90,
        borderRadius:10,
        textAlign:'center',
        marginHorizontal:10,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent:'center'
    }
});
