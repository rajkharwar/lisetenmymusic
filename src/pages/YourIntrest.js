import React,{Component} from 'react';
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import { StyleSheet, View, Platform, Text, FlatList, TouchableOpacity, Alert, ActivityIndicator, Image } from 'react-native';
import axios from "axios";
import MusicList from "./MusicList";
import HomeScrolls from "./HomeScrolls";

import { createAppContainer, createSwitchNavigator } from 'react-navigation';

const numColumns = 3;
const size = Dimensions.get('window').width/numColumns;
 export default class YourIntrest extends React.Component
{
    constructor() {
        super();
        this.state={interests:[],selectedInterest:[]};
    }
    _onPressButton=()=>{
       this.props.navigation.navigate('MusicList',{interests:this.state.interests})
    }
    selectInterest= (s)=> {
        const players = this.state.selectedInterest;
        players.push(s);
        this.setState({
            selectedInterest: players,
        });
    }
    componentDidMount() {
        axios({
            method: 'GET',
            url: 'http://listen-my-story.azurewebsites.net/api/Login/GetAllInterest?ClientId=0',
        })
            .then((response) => {

                this.setState({interests:response.data})

            }, (error) => {
                console.log(error);
                alert(error);
            });

    }
    render() {
        return (

           <View style={styles.container}>

               <Image source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />

               <View style={styles.textView}>
                   <View style={styles.RecoverPass}>
                       <TouchableOpacity>
                           <Image
                               source={require('../images/arrow.png')}
                           />
                       </TouchableOpacity>
                       <Text style={{color:'#ffffff',fontSize:20,paddingHorizontal:110}}>Your Interest</Text>
                   </View>
                   <FlatList

                       contentContainerStyle={styles.list}

                       data={this.state.interests}
                       renderItem={({item}) => (
                           <View style={[styles.list]}>
                               <TouchableOpacity onPress={() => this.selectInterest(item)}>
                               <View >
                                   <Text style={styles.item}>{item.Name}</Text>
                               </View>
                               </TouchableOpacity>
                           </View>
                       )}
                       keyExtractor={item => item.id}
                   />
                   <TouchableOpacity onPress={this._onPressButton} style={{
                       marginTop: 70,
                       flexDirection: 'row',
                       height: 50,
                       justifyContent: 'center',
                       backgroundColor: '#EC4187',
                       paddingTop: 15,
                       borderRadius: 10,
                   }}>
                       <Text style={{color:'#ffffff'}}>Submit</Text>
                   </TouchableOpacity>
               </View>

                    </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        //flexDirection:'row',
        //height:"100%",
        //width:"100%"
    },

    backdrop: {
        flex:1,
        //flexDirection: 'row',
        //height:'100%',
        width: '100%',
        //position:'absolute',
      //  alignItems: 'center',
       // justifyContent:'center'
    },

    list: {
       // justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',



    },
    item:{
        flex: 1,
        margin: 5,
        marginTop: 35,
        minWidth: 100,
        maxWidth: 100,
        height: 100,
       // maxHeight: 200,
        backgroundColor: '#ffffff',
        color:'#000000',
        flexDirection:'column',
        borderRadius:10,
        textAlign:'center',
        paddingTop:40,



    },
    textView:{
        position: 'absolute',
        marginTop:20,
        paddingHorizontal:20,


    },
    RecoverPass:{
        flexDirection: 'row',
        justifyContent:'space-between',

    }
});

// export default InitialNavigator;