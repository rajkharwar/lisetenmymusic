import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar, Image, ImageBackground, ScrollView,
    TouchableOpacity
} from 'react-native';
import Header from './Header';
import AlbumArt from './AlbumsArt';
import TrackDetails from './TrackDetails';
import SeekBar from './SeekBar';
import Controls from './Controls';
import Video from 'react-native-video';
import Slider from 'react-native-slider/lib/Slider';
import Login from './Login';
import MusicDescription from './MusicDescription';
import MusicRating from './MusicRating';
//
// export const TRACKS = [
//     {
//         title: '101 - The Random Thoughts',
//         artist: 'Twenty One Pilots',
//         albumArtUrl: "http://36.media.tumblr.com/14e9a12cd4dca7a3c3c4fe178b607d27/tumblr_nlott6SmIh1ta3rfmo1_1280.jpg",
//         audioUrl: "https://djobs.file.core.windows.net/djob/djob/001_SONG.mp3?sv=2018-03-28&sr=f&si=DjobFileSharePolicy&sig=Sb7CSnX3D1YXnhUaauIH9H%2BQr4DXoE9jRw6ncdvDjpM%3D",
//     },
//     {
//         title: 'Love Yourself',
//         artist: 'Justin Bieber',
//         albumArtUrl: "http://arrestedmotion.com/wp-content/uploads/2015/10/JB_Purpose-digital-deluxe-album-cover_lr.jpg",
//         audioUrl: 'https://djobs.file.core.windows.net/djob/djob/002_SONG.mp3?sv=2018-03-28&sr=f&si=DjobFileSharePolicy&sig=kCgoCit8twWFXRKR3oob2ZYZq8KAS6I%2B7%2Bxu7ysDet8%3D',
//     },
//     {
//         title: 'Hotline Bling',
//         artist: 'Drake',
//         albumArtUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/c9/Drake_-_Hotline_Bling.png',
//         audioUrl: 'http://dl2.shirazsong.org/dl/music/94-10/CD%201%20-%20Best%20of%202015%20-%20Top%20Downloads/03.%20Drake%20-%20Hotline%20Bling%20.mp3',
//     },
// ];
export default class Player extends Component {
    constructor(props) {
        super(props);

        this.state = {
            paused: true,
            totalLength: 1,
            currentPosition: 0,
            selectedTrack: 0,
            repeatOn: false,
            shuffleOn: false,
            //tracks:""
        };
    }

    setDuration(data) {
        // console.log(totalLength);
        this.setState({totalLength: Math.floor(data.duration)});
    }

    setTime(data) {
        //console.log(data);
        this.setState({currentPosition: Math.floor(data.currentTime)});
    }

    seek(time) {
        time = Math.round(time);
        this.refs.audioElement && this.refs.audioElement.seek(time);
        this.setState({
            currentPosition: time,
            paused: false,
        });
    }

    onBack() {
        if (this.state.currentPosition < 10 && this.state.selectedTrack > 0) {
            this.refs.audioElement && this.refs.audioElement.seek(0);
            this.setState({ isChanging: true });
            setTimeout(() => this.setState({
                currentPosition: 0,
                paused: false,
                totalLength: 1,
                isChanging: false,
                selectedTrack: this.state.selectedTrack - 1,
            }), 0);
        } else {
            this.refs.audioElement.seek(0);
            this.setState({
                currentPosition: 0,
            });
        }
    }

    onForward() {
        if (this.state.selectedTrack < this.props.tracks.length - 1) {
            this.refs.audioElement && this.refs.audioElement.seek(0);
            this.setState({ isChanging: true });
            setTimeout(() => this.setState({
                currentPosition: 0,
                totalLength: 1,
                paused: false,
                isChanging: false,
                selectedTrack: this.state.selectedTrack + 1,
            }), 0);
        }
    }



    render() {
        const ratingObj = {
            ratings: 3,
            views: 34000
        }
         const StoryURL = this.props.navigation.state.params.tracks.StoryURL;
        const StoryImage = this.props.navigation.state.params.tracks.StoryImage;
        //console.log(StoryURL);
        //  const {StoryImage} = this.props.tracks.StoryImage;
        //  const {StoryName} = this.props.tracks.StoryName;
        // const {StoryURL} = this.props.state.tracks.StoryURL;
        // const {Description} = this.props.state.tracks.Description;
        //track.getAllKeys();
        //const {StoryURL} = track;
       // const track = this.props.tracks[2];
        //alert(track.StoryURL);
        const video = this.state.isChanging ? null : (

            <Video source={{uri: StoryURL}} // Can be a URL or a local file.
                   ref="audioElement"
                   paused={this.state.paused}               // Pauses playback entirely.
                   resizeMode="cover"           // Fill the whole screen at aspect ratio.
                   repeat={true}                // Repeat forever.
                   onLoadStart={this.loadStart} // Callback when video starts to load
                   onLoad={this.setDuration.bind(this)}    // Callback when video loads
                   onProgress={this.setTime.bind(this)}    // Callback every ~250ms with currentTime
                   onEnd={this.onEnd}           // Callback when playback finishes
                   onError={this.videoError}    // Callback when video cannot be loaded
                   style={styles.audioElement}
                   playInBackground={true}
                   playWhenInactive={true}
            />
        );

        return (
            <ScrollView>
            <View style={styles.container}>


                <View style = {styles.backgroundContainer}>
                    <ImageBackground source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />
                </View>
                <StatusBar hidden={true} />
                <Header message="" />
                <AlbumArt url={StoryImage} />
                <TrackDetails title={this.props.navigation.state.params.StoryName} artist={'LODU'} />

                <SeekBar
                    onSeek={this.seek.bind(this)}
                    trackLength={this.state.totalLength}
                    onSlidingStart={() => this.setState({paused: true})}
                    currentPosition={this.state.currentPosition}
                />

                <Controls
                    onPressRepeat={() => this.setState({repeatOn : !this.state.repeatOn})}
                    repeatOn={this.state.repeatOn}
                    shuffleOn={this.state.shuffleOn}
                    forwardDisabled={this.state.selectedTrack === this.props.navigation.state.params.length - 1}
                    onPressShuffle={() => this.setState({shuffleOn: !this.state.shuffleOn})}
                    onPressPlay={() => this.setState({paused: false})}
                    onPressPause={() => this.setState({paused: true})}
                    onBack={this.onBack.bind(this)}
                    onForward={this.onForward.bind(this)}
                    paused={this.state.paused}/>
                    {video}

                    <MusicDescription/>

            </View>
            </ScrollView>


        );
    }
}

const styles = {
    container: {
        flex: 1,
       //backgroundColor: 'rgb(4,4,4)',
    },
    audioElement: {
        height: 0,
        width: 0,
    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    backdrop: {
        flex:1,
        flexDirection: 'column'
    },
};
