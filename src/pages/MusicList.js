import React from 'react';
import {StyleSheet, FlatList, Text, View, Alert, Image, TouchableOpacity, ImageBackground} from 'react-native';
import {navigate} from 'react-navigation';
import MainPage from './MainPage';
import axios from 'axios';
import HomeScrolls from './HomeScrolls';
//import TabNavigations from './TabNavigations';


class MusicList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            allSongs: []
        }

    }


    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#000",
                }}
            />
        );
    };

    onClickButton = (tracks) =>{
        this.props.navigation.navigate('Player',{tracks});
    }
    componentWillMount(): void {
        axios({
            method: 'GET',
            url: 'http://listen-my-story.azurewebsites.net/api/Story/GetAllStory?ClientId=1&PageCount=1',


        })
            .then((response) => {

                // alert(JSON.parse(response));
                const allSongs = response.data;
                this.setState({allSongs})
                //alert(allSongs.StoryId)
            }, (error) => {
                console.log(error);
                alert(error);
            });
    }

    render() {



        const {allSongs} = this.state;

        return (

            <View style={styles.container}>
                <View>
                <View style = {styles.backgroundContainer}>
                    <ImageBackground source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />
                </View>
                <View style={{flexDirection:'row',marginTop:30, marginHorizontal:10,alignItems:'center'}}>
                    <Image source={require('../images/Bitmap.png')} style={{height:100,width:100,borderRadius:10}}/>
                    <View>
                    <Text style={{color:'#FFFFFF',fontSize:18,fontWeight:'bold',marginHorizontal:10,marginBottom:12,letterSpacing:0.8}}>Hi Sarath,</Text>

                    <Text style={{color:'#FFFFFF',marginHorizontal:10,letterSpacing:0.8}}>Start listen music of your interest</Text>
                    </View>
                </View>
                <View style={{marginTop:10}}>
                    <HomeScrolls temp={this.props.navigation.state.params.interests}/>
                    <View>
                    <Text style={{color:'#FFFFFF',fontSize:25,marginHorizontal:10,letterSpacing:0.8}}>
                        Stories
                    </Text>
                    </View>
                </View>



                </View>

                <FlatList
                    data={[
                        {'101': 'The Random Thoughts'},{'102': 'adkj'}, {'103': 'aslkl'},{'104': 'Swift'},
                        {'105': 'Php'},{'106': 'Hadoop'},{'107': 'Sap'},

                    ]}
                    renderItem={({item}) =>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                        {/*<View style={{height: 70,flexDirection:'row'}}>*/}
                            {/*<TouchableOpacity onPress={this._onPressButton} >*/}
                            {/*    <Image source={require('../images/Rectangle3.png')} style={{height:50,width:50,marginTop:10,marginLeft:20}}/>*/}
                            {/*</TouchableOpacity>*/}

                                 {
                                     this.state.allSongs.map(user => {

                                             const {StoryId,StoryName,Description,StoryURL,StoryImage} = user;
                                             const d = {StoryId,StoryName,Description,StoryURL,StoryImage};
                                             return (

                                                 <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                                                     <View style={{height: 70,flexDirection:'row'}}>
                                                     <TouchableOpacity onPress={() => this.onClickButton(d)} >
                                                         <Image source={{uri:d.StoryImage}} style={{height:50,width:50,marginTop:10,marginLeft:20}}/>
                                                     </TouchableOpacity>

                                                     <Text style={styles.item}>
                                                         {d.StoryName}
                                                     </Text>
                                                 </View>
                                                 </View>
                                             );
                                         }

                                     )
                                 }




                             <View style={{flexDirection:'row',marginRight:30}}>
                            <Image source={require('../images/options.png')} style={{height:30,width:30,marginLeft:8}}/>
                            <Image source={require('../images/download2.png')} style={{height:30,width:30,marginLeft:8}}/>
                            <Image source={require('../images/Remove.png')} style={{height:30,width:30,marginLeft:8}}/>
                             </View>
                        </View>}
                    keyExtractor={item => item.key}
                    ItemSeparatorComponent={this.renderSeparator}
                />




            </View>

        );

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,

      //  backgroundColor: '#FFFFFF'
    },
    item: {
        textAlign:'center',
        paddingTop: 25,
        fontSize: 15,
        paddingHorizontal:10,
       // height: 44,

    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    backdrop: {
        flex:1,
       flexDirection: 'column'
    },
});
export default MusicList;
