// // import React, {Component} from 'react';
// // import {
// //     StatusBar,
// //     View,
// //     Text,
// //     ActivityIndicator,
// //     StyleSheet,
// //     ImageBackground,
// //     Image
// // } from 'react-native';
// // export default class HomeScreen extends React.Component<{}> {
// //     render() {
// //         return (
// //             <View style={styles.container}>
// //
// //                 <ImageBackground source={require('../images/HomeScreen.png')} style={styles.backgroundImage}
// //                       />
// //                 <Image style = {styles.logo} source = {require('../images/Logo.png')} />
// //
// //             </View>
// //
// //
// //
// //
// //
// //     );
// //     }
// // }
// //
// // const styles = StyleSheet.create({
// //     container : {
// //         flex: 1,
// //         justifyContent: 'center',
// //         alignItems: 'center',
// //     },
// //     backgroundImage:{
// //         height:'100%',
// //         width:'100%'
// //
// //     },
// //
// //     logo: {
// //        // marginVertical: 19,
// //
// //     },
// //
// // });
//
//
import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
    ImageBackground
} from 'react-native';
export default class HomeScreen extends React.Component {
    render() {
        return (
//             <View style={styles.container}>
//                 <Image source={require('../images/HomeScreen.png')}
//                        style={{width: '100%', height: '100%'}}
//                     />
//                 <Text style={styles.welcomeText}> Easy To get a job </Text>
//                 <Text style={styles.welcometxt}> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </Text>
//             </View>

<View style={styles.container}>
    <View style = {styles.backgroundContainer}>
        <ImageBackground source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />
    </View>
    <View>
        <Image style = {styles.logo} source = {require('../images/Logo.png')} />
    </View>
</View>





        );
    }
}

// const styles = StyleSheet.create({
//     container : {
//         flex: 1,
//
//     },
//     welcomeText : {
//         marginVertical: 19,
//         color :'#00CB99',
//         fontSize : 20,
//     },
//     welcometxt : {
//         justifyContent: 'center',
//         alignItems: 'center',
//         paddingHorizontal: 25,
//         color: '#243164',
//         fontSize: 15,
//     }
// });


var styles = StyleSheet.create({
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },

    logo: {
      //  backgroundColor: 'rgba(0,0,0,0)',
        width: 100,
        height: 100,
        marginTop:300,

    },
    backdrop: {
        flex:1,
        flexDirection: 'column'
    },
    headline: {
        fontSize: 18,
        textAlign: 'center',
        backgroundColor: 'black',
        color: 'white'
    }
});
