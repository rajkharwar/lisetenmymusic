import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text, TouchableOpacity
} from 'react-native';
import { AirbnbRating } from 'react-native-ratings';
export default class MusicRating extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.textView}>
                        <Text style={{color:'#ffffff',fontSize:20,paddingBottom:10}}>101 - The Random Thoughts</Text>
                </View>
                <View style={{
                    flexDirection: 'row',
                    height: 110,
                    paddingHorizontal:20,
                    backgroundColor: '#ffffff',
                    paddingTop: 10,
                    borderRadius: 10,
                    marginLeft:10,
                    marginRight:17
                }}>
                    <View title="DEFAULT">
                        <Text style={{fontSize:18,color:'#5E6A6E',paddingBottom:10}}>Rate This Product</Text>
                        <AirbnbRating showRating={false} />
                    </View>
                </View>
            </View>


        );
    }

    _valueChanged(rating) {
        console.log(rating);
    }
}


var styles = StyleSheet.create({
    container: {
        flex: 1,
    },


    textView:{
       // position: 'absolute',
       // marginTop:20,
        paddingHorizontal:20,
    },
    textStyle:{
        fontSize: 20,
        color: '#ffffff',
        marginTop:30
    },


    RecoverPass:{
        flexDirection: 'row',
        justifyContent:'space-between',

    }


});
