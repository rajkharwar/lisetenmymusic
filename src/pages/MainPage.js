
import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
    ImageBackground, Alert
} from 'react-native';
import HomeScrolls from './HomeScrolls';
import MusicList from './MusicList';
import axios from "axios";
let arr ={};
export default class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.state={interests:[]};
    }

    componentDidMount() {
        axios({
            method: 'GET',
            url: 'https://listen-my-story.azurewebsites.net/api/Login/GetAllInterest?ClientId=0',


        })
            .then((response) => {
                const allSongs = response.data;
                this.setState({interests:response.data})

            }, (error) => {
                console.log(error);
                alert(error);
            });

    }
    render() {
        return (
            <View style={styles.container}>
                <View style = {styles.backgroundContainer}>
                    <ImageBackground source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />
                </View>
                <View style={{flexDirection:'row',marginTop:30,marginLeft:20,alignItems:'center'}}>
                 <Image source={require('../images/Bitmap.png')} style={{height:100,width:100,}}/>
                <Text style={{color:'#FFFFFF'}}>Hi Sarath,</Text>
                    <Text style={{color:'#FFFFFF'}}>Start listen music of your interest</Text>
                </View>
                <View style={{marginTop:10}}>
                <HomeScrolls temp={this.state.interests}/>
                </View>
                <Text style={{color:'#FFFFFF',fontSize:25}}>
                    Stories
                </Text>
                <MusicList  />
            </View>






        );
    }
}



var styles = StyleSheet.create({
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    container: {
        flex: 1,

    },
    backdrop: {
        flex:1,
        flexDirection: 'column'
    },
});
