import React from "react";
import { StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity } from "react-native";
import Slider from "react-native-slider";
import Moment from "moment";
import SoundPlayer from 'react-native-sound-player';


export default class MusicTrack extends React.Component{

    state = {
        trackLength: 300,
        timeElapsed: "0:00",
        timeRemaining: "5:00"
    };

    changeTime = seconds => {
        this.setState({ timeElapsed: Moment.utc(seconds * 1000).format("m:ss") });
        this.setState({ timeRemaining: Moment.utc((this.state.trackLength - seconds) * 1000).format("m:ss") });
    };
    changeToSeek = seconds => {
        SoundPlayer.seek( Moment.utc(seconds * 1000).format("m:ss"));
        SoundPlayer.play()
    };
    render() {
        return(
            <SafeAreaView style={styles.container}>
            {/*<View style={{marginTop:200}}>*/}
                <Slider
                    minimumValue={0}
                    maximumValue={this.state.trackLength}
                    trackStyle={styles.track}
                    thumbStyle={styles.thumb}
                    minimumTrackTintColor="#00CB99"
                    onValueChange={seconds => this.changeTime(seconds)}
                    // onSlidingComplete={onSeek}
                ></Slider>

                {/*<View style={{ marginTop: -12, flexDirection: "row", justifyContent: "space-between" }}>*/}
                    <TouchableOpacity onPress={SoundPlayer.playUrl('https://djobs.file.core.windows.net/djob/djob/001_SONG.mp3?sv=2018-03-28&sr=f&si=DjobFileSharePolicy&sig=Sb7CSnX3D1YXnhUaauIH9H%2BQr4DXoE9jRw6ncdvDjpM%3D')} >

                    <Text style={[styles.textLight, styles.timeStamp]}>{this.state.timeElapsed}</Text>
                    <Text style={[styles.textLight, styles.timeStamp]}>{this.state.timeRemaining}</Text>
                    </TouchableOpacity>
                {/*</View>*/}

            {/*</View>*/}
            </SafeAreaView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textLight: {
        color: "#B6B7BF"
    },
    text: {
        color: "#8E97A6"
    },
    textDark: {
        color: "#3D425C"
    },
    coverContainer: {
        marginTop: 32,
        width: 250,
        height: 250,
        shadowColor: "#5D3F6A",
        shadowOffset: { height: 15 },
        shadowRadius: 8,
        shadowOpacity: 0.3
    },
    cover: {
        width: 250,
        height: 250,
        borderRadius: 125
    },
    track: {
        height: 3,
        borderRadius: 1,
        backgroundColor: "#D8D8D8"
    },
    thumb: {
        width: 8,
        height: 8,
        backgroundColor: "yellow"
    },
    timeStamp: {
        fontSize: 11,
        fontWeight: "500"
    },

});
