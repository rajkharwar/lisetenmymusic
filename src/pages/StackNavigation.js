import  Login  from './Login';
import Player from './Player';
import MusicList from './MusicList';
import MainPage from './MainPage';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SignUp from './SignUp';
import {createBottomTabNavigator} from 'react-navigation-tabs';
const StackNavig = createStackNavigator(

    {

        //Login: { screen: Login },
        //MainPage: { screen: MainPage},

        MusicList:{screen:MusicList},
        Player :{screen:Player},
        //SignUp :{screen:SignUp}
    },
    {
        initialRouteName: 'MusicList',
        headerMode: 'none'
    }
)
export default createAppContainer(StackNavig);
