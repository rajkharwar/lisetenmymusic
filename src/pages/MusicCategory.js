import React,{Component} from 'react';
import { Dimensions , ScrollView} from 'react-native';
const { width, height } = Dimensions.get('window');
import { StyleSheet, View, Platform, Text, FlatList, TouchableOpacity, Alert, ActivityIndicator, Image } from 'react-native';

import {Button, Card} from 'react-native-elements';
//import CardView from 'react-native-cardview';
const data = [
    {id: 'a', value: 'A'},
    {id: 'b', value: 'B'},
    {id: 'c', value: 'C'},
    {id: 'd', value: 'D'},
    {id: 'e', value: 'E'},
    {id: 'f', value: 'F'},
];
const gutter = 0;

const numColumns = 3;
const size = Dimensions.get('window').width/numColumns;
export default class MusicCategory extends Component
{

    render() {
        return (

            <View style={styles.container}>
                <ScrollView>

                    <View style={styles.textView}>

                        <FlatList

                            contentContainerStyle={styles.list}

                            data={[
                                { key: "Love"},
                                { key: 'Life' },
                                { key: 'Relations' },
                                { key: 'Education' },
                                { key: 'Drama' },
                                { key: 'Politics' },
                                { key: 'Happy & Sad' },

                            ]}
                            renderItem={({item}) => (
                                <View style={[styles.viewItem]}>
                                    {/*<View>*/}
                                    {/*    <Text style={styles.item}>{item.key}</Text>*/}

                                    {/*    <Text style={{color:'#ffffff'}}>{item.value}</Text>*/}


                                    {/*</View>*/}

                                    {/*<Card*/}
                                    {/*    image={require('../component/images/drawer.png')}>*/}
                                    {/*    <Text style={{marginBottom:25}}>*/}
                                    {/*        The idea with React Native Elements is more about component structure than actual design.*/}
                                    {/*    </Text>*/}

                                    {/*<Button*/}

                                    {/*    buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}*/}
                                    {/*    title='VIEW NOW' />*/}

                                    {/*</Card>*/}

                                    <View style={{width: (Dimensions.get('window').width - 30) / 2,
                                        height: (Dimensions.get('window').width - 30) / 2 }}>
                                        <Image
                                            style={{ width: (Dimensions.get('window').width - 30) / 2,
                                                height: (Dimensions.get('window').width - 30) / 2,position:'absolute',resizeMode:'cover' }}
                                            source={require('../images/Rectangle3.png') }
                                        />
                                    </View>
                                    <View style={{flex:1}}>
                                        <Text style={{ color: 'black', fontSize: 18, margin: 6 }}>{'Title'}</Text>
                                        <Text style={{ color: 'black', margin: 6 }}>{'Subtitle'}</Text>
                                    </View>
                                </View>
                            )} keyExtractor={item => item.id}

                        />

                    </View>
                </ScrollView>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        //  alignItems: 'stretch',
        backgroundColor: '#E82743',
        //flexDirection:'row',
        //height:"100%",
        //width:"100%"
        //flexDirection:'column'
    },
    list: {
        // justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent:'space-between'
    },
    viewItem:{
        //flex:1,
        margin:18,
        maxWidth: 140,
        maxHeight: 200,
        flexDirection:'column',
        justifyContent:'center',
        //alignItems:'center'
        width: (Dimensions.get('window').width),
        //marginBottom:5
        //height: (Dimensions.get('window').height),
    },
    textView:{

        //marginTop:20,
        paddingHorizontal:20,
        //justifyContent:'center',

    },
    RecoverPass:{
        flexDirection: 'row',
        justifyContent:'space-between',

    }
});
