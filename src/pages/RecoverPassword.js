import React, {Component} from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity

} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';

export default class RecoverPassword extends React.Component {

    render() {
        return (

            <View style={styles.container}>

                <Image source={require('../images/HomeScreen.png')} resizeMode = 'cover' style = {styles.backdrop} />

                <View style={styles.textView}>
                    <View style={styles.RecoverPass}>
                        <TouchableOpacity>
                            <Image
                                source={require('../images/arrow.png')}
                            />
                        </TouchableOpacity>
                        <Text style={{color:'#ffffff',fontSize:20,paddingHorizontal:90}}>Recover Password</Text>
                    </View>
                    <Text style={styles.textStyle}>Registered E-mail id to be used to recover your password</Text>
                    <Text style={styles.textlabel}>E-mail id</Text>
                    <TextInput style={styles.inputtext}    placeholder = "E-mail address " underlineColorAndroid='transparent'/>
                    <TouchableOpacity onPress={this._onPressButton} style={{
                        marginTop: 320,
                        flexDirection: 'row',
                        height: 50,
                        justifyContent: 'center',
                        backgroundColor: '#EC4187',
                        paddingTop: 15,
                        borderRadius: 30,
                    }}>
                        <Text style={{color:'#ffffff'}}>Send verification Link</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}


var styles = StyleSheet.create({
    backgroundContainer: {
        position: 'relative',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

    },
    container: {
        flex: 1,
        alignItems: 'center',

    },

    backdrop: {
        flex:1,
        width: '100%',

    },

    textView:{
        position: 'absolute',
        marginTop:20,
        paddingHorizontal:20,
    },
    textStyle:{
        fontSize: 20,
        color: '#ffffff',
        marginTop:30
    },
    inputtext:{
        marginTop:10,
        height: 50,
        borderColor: '#ffffff',
        backgroundColor: '#ffffff',
        borderWidth: 1,
        borderRadius:10,
        fontSize: 18,
        paddingHorizontal : 16,

    },
    textlabel:{
        marginTop:50,
        fontSize:15,
        color:'#ffffff',
        fontStyle:'normal'
    },
    RecoverPass:{
        flexDirection: 'row',
        justifyContent:'space-between',

    }


});
