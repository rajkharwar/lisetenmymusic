//
// import React, {Component} from 'react';
// import {
//
//   StyleSheet,
//   View,
//   Text,
// } from 'react-native';
//
// import HomeScreen from './src/pages/HomeScreen';
// import Login from './src/pages/Login';
// import RecoverPassword from './src/pages/RecoverPassword';
// import SignUp from './src/pages/SignUp';
// import YourIntrest from './src/pages/YourIntrest';
// import Setting from './src/pages/Setting';
// import MusicTrack from './src/pages/MusicTrack';




//
// export default class App extends Component<{}>{
//   render(){
//     return(
//         <View style={styles.container}>
//
//           <MusicTrack/>
//         </View>
//
//     );
//   }
// }
// const styles = StyleSheet.create({
//   container:{
//     flex:1,
//
//
//   }
//
// });
//
//


import React, { Component } from 'react';
import Player from  './src/pages/Player';
import {View} from 'react-native';
import MusicList from './src/pages/MusicList';
import MusicTrack from './src/pages/MusicTrack';
import MainPage from './src/pages/MainPage';
import TabNavigation from './src/pages/TabNavigation';
import TabNavigations from './src/pages/TabNavigations';
import GeneralStarExample from './src/pages/MusicRating';
import YourIntrest from "./src/pages/YourIntrest";
//import MusicCategory from './src/pages/MusicCategory';
export const TRACKS = [
  {
    title: '101 - The Random Thoughts',
    artist: 'Twenty One Pilots',
    albumArtUrl: "http://36.media.tumblr.com/14e9a12cd4dca7a3c3c4fe178b607d27/tumblr_nlott6SmIh1ta3rfmo1_1280.jpg",
    audioUrl: "https://djobs.file.core.windows.net/djob/djob/001_SONG.mp3?sv=2018-03-28&sr=f&si=DjobFileSharePolicy&sig=Sb7CSnX3D1YXnhUaauIH9H%2BQr4DXoE9jRw6ncdvDjpM%3D",
  },
  {
    title: 'Love Yourself',
    artist: 'Justin Bieber',
    albumArtUrl: "http://arrestedmotion.com/wp-content/uploads/2015/10/JB_Purpose-digital-deluxe-album-cover_lr.jpg",
    audioUrl: 'https://djobs.file.core.windows.net/djob/djob/002_SONG.mp3?sv=2018-03-28&sr=f&si=DjobFileSharePolicy&sig=kCgoCit8twWFXRKR3oob2ZYZq8KAS6I%2B7%2Bxu7ysDet8%3D',
  },
  {
    title: 'Hotline Bling',
    artist: 'Drake',
    albumArtUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/c9/Drake_-_Hotline_Bling.png',
    audioUrl: 'http://dl2.shirazsong.org/dl/music/94-10/CD%201%20-%20Best%20of%202015%20-%20Top%20Downloads/03.%20Drake%20-%20Hotline%20Bling%20.mp3',
  },
];
//import MusicList from './src/pages/MusicList'
//import InitialNavigator from './src/pages/YourIntrest';
import HomeScrolls from "./src/pages/HomeScrolls";
import {createSwitchNavigator,createAppContainer} from "react-navigation";

const InitialNavigator = createSwitchNavigator(
    {
      YourInterest:YourIntrest,
      MusicList:MusicList,

    },
    {
      InitialRouteName: 'YourInterest',
    }

);
const AppContainer = createAppContainer(InitialNavigator);
export default class App extends Component {
  render() {

    return(

             <AppContainer/>


    );



  }
}
